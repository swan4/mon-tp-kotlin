fun start(){
    "Ok"
}
fun joinOptions(options: Collection<String>) = options.joinToString(separator = ",", prefix= "[", postfix= "]")
class Person(val name: String, val age: Int)
fun main(){
    println(start())
//    >> Ok
    println(joinOptions(listOf("a", "b", "C", "uyguygu")))
//    [a,b,C,uyguygu]
    val person = Person("Swan", 34)
    println(person.name)
//    Swan
    println(person.age)
// 34
}