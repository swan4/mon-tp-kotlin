data class Shop(val name: String, val customers:
List<Customer>){
    fun getCitiesCustomersAreFrom() = customers.map {it.city.name}
    fun getCustomersFrom(cityName: String) = customers.filter {it.city.name == cityName}
    fun checkAllCustomersAreFrom(city: City): Boolean = customers.all {it.city == city}
    fun countCustomersFrom(city: City): Int = customers.count{it.city == city}
    fun findAnyCustomerFrom(city: City): Customer? = if (customers.any {it.city == city}) customers.find {it.city == city} else null
    fun getCustomerWithMaximumNumberOfOrders(): Customer? = customers.maxByOrNull { it.orders.count() }
    fun groupCustomersByCity(): Map<City, List<Customer>> = customers.groupBy {it.city}
    fun getNumberOfTimesProductWasOrdered(product: Product): Int =  customers.flatMap { it.getOrderedProductsList()}.count {it == product}


}
data class Customer(val name: String, val city: City, val
orders: List<Order>) {
    override fun toString() = "$name from ${city.name}"
    fun getOrderedProductsList(): List<Product> = orders.flatMap {it.products}
    fun getMostExpensiveOrderedProduct(): Product? = getOrderedProductsList().maxByOrNull {it.price}
    fun getMostExpensiveDeliveredProduct(): Product? = orders.filter {it.isDelivered}.flatMap {it.products}.maxByOrNull {it.price}
    fun getMostExpensiveOrderedProduct(): Product? = orders.flatMap {it.products}.maxByOrNull {it.price}
}
data class Order(val products: List<Product>, val
isDelivered: Boolean)
data class Product(val name: String, val price: Double) {
    override fun toString() = "'$name' for $price"
}
data class City(val name: String) {
    override fun toString() = name
}
fun main() {
    val products = listOf(Product("banane", 32.2), Product("TV HD", 300.0))
    val orders = listOf(Order(products, true))
    val customer1 = Customer("jack", City("Macon"), orders);
    val customer2 = Customer("jack", City("Lyon"), orders);
    val customer3 = Customer("jack", City("Crest"), orders);
    val customer4 = Customer("chri-chri", City("Crest"), , listOf(Order(products, true), Order(products, false));
    val customer5 = Customer("chri-chri", City("Crest"), listOf(Order(listOf(Product("pomme granny", 2.0)), true), Order(listOf(Product("piscine", 1000.0)), false)));
    val shop = Shop("Chez bibi", listOf(customer1, customer2, customer3))
    val shop2 = Shop("Chez jean", listOf( customer4, customer3))
    println(shop.getCitiesCustomersAreFrom())
//    [Macon, Lyon, Crest]
    println(shop.getCustomersFrom("Crest"))
//    [jack from Crest]
    println(shop.checkAllCustomersAreFrom(City("Macon")))
//    false
    println(shop2.checkAllCustomersAreFrom(City("Crest")))
//    true

    println(shop2.countCustomersFrom(City("Crest")))
//    2
    println(shop.countCustomersFrom(City("Lyon")))
//    1
    println (shop.findAnyCustomerFrom(City("Macon")))
//    jack from Macon
    println (shop.findAnyCustomerFrom(City("Arles")))
//    null
    println(shop2.getCustomerWithMaximumNumberOfOrders())
// chri-chri from Crest
    println(customer1.getMostExpensiveOrderedProduct())
// 'TV HD' for 300.0
    println( shop.groupCustomersByCity())
// {Macon=[jack from Macon], Lyon=[jack from Lyon], Crest=[jack from Crest]}
    println(customer1.getMostExpensiveDeliveredProduct())
//  'TV HD' for 300.0
    println(customer5.getMostExpensiveDeliveredProduct())
// pomme granny' for 2.0
    println(shop.getNumberOfTimesProductWasOrdered(Product("banane", 32.2)))
// 3
}